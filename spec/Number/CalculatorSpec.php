<?php

namespace spec\Number;

use Number\Calculator;
use Operator\Adder;
use Operator\Divider;
use Operator\Multiplier;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CalculatorSpec extends ObjectBehavior
{
    function let(
      Adder $adder,
      Divider $divider,
      Multiplier $multiplier
    ) {
        $this->beConstructedWith($adder, $multiplier, $divider);
    }

    function it_adds_two_numbers(Adder $adder)
    {
        $adder->calculate(1,2)->willReturn(3);
        $adder->calculate(3,4)->willReturn(7);

        $this->add(1, 2)->shouldReturn(3);
        $this->add(3, 4)->shouldReturn(7);
    }

    function it_multiplies_two_numbers()
    {
        $this->multiply(1, 2)->shouldReturn(2);
        $this->multiply(3, 4)->shouldReturn(12);
        $this->multiply(-3, 4)->shouldReturn(-12);
        $this->multiply(-3, 0)->shouldReturn(0);
    }

    function it_divides_two_numbers()
    {
        $this->divide(2, 1)->shouldReturn(2);
        $this->divide(3, 4)->shouldReturn(0.75);
        $this->divide(-3, 4)->shouldReturn(-0.75);
    }

    function it_not_allows_to_divide_by_zero()
    {
        $this->shouldThrow(new \LogicException('Do not divide by zero!'))
            ->duringDivide(Argument::type('int'), 0);
    }
}
