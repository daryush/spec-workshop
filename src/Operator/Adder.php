<?php

namespace Operator;

interface Adder
{

    public function calculate($argument1, $argument2);
}
