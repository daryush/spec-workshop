<?php

namespace Number;

use Operator\Adder;
use Operator\Divider;
use Operator\Multiplier;

class Calculator
{
    /**
     * @var Adder
     */
    private $adder;
    /**
     * @var Divider
     */
    private $divider;
    /**
     * @var Multiplier
     */
    private $multiplier;

    public function __construct(
        Adder $adder,
        Multiplier $multiplier,
        Divider $divider
    ) {
        $this->adder = $adder;
        $this->divider = $divider;
        $this->multiplier = $multiplier;
    }

    public function add($numberOne, $numberTwo)
    {
        return $this->adder->calculate($numberOne, $numberTwo);
    }

    public function multiply($numberOne, $numberTwo)
    {
        return $numberTwo * $numberOne;
    }

    public function divide($numberOne, $numberTwo)
    {
        if ($numberTwo === 0) {
            throw new \LogicException('Do not divide by zero!');
        }

        return $numberOne / $numberTwo;
    }
}
